docker run \
  --volume=/var/lib/sss/pipes/:/var/lib/sss/pipes/:rw  \
  --volume=/etc/sssd/:/etc/sssd/:ro  \
  --volume=/etc/krb5.conf:/etc/krb5.conf:ro \
  --volume=/etc/ipa/ca.crt:/etc/ipa/ca.crt:ro  \
  --volume=/etc/nsswitch.conf:/etc/nsswitch.conf:ro \
  --volume=/etc/pam.d/:/etc/pam.d/:ro \
  -it \
  sssd:latest
