FROM debian:buster-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    libnss-sss libpam-sss

ENTRYPOINT ["/bin/bash"]
