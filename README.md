# sssd

Use sssd inside a container. This is based on techniques described in:

https://www.arctiq.ca/our-blog/2017/3/29/are-your-linux-containers-having-an-identity-crisis/
https://jhrozek.wordpress.com/2015/03/31/authenticating-a-docker-container-against-hosts-unix-accounts/

docker.sh is just a test file for invoking a shell with the appropriate host
directories mounted.  Normally, I would do this in Kubernetes. 

Note that if you want different settings in /etc/sssd/sssd.conf, you can specify
those directly, rather than mounting from the host.
